<?php
$servername = "localhost";
$username = "root";
$password = "Admin123.";
$dbname = "albums";

$conn = new mysqli($servername, $username, $password, $dbname);

if (isset($_POST['novaReproduccio'])) {
    $idUsuari = rand(1, 7);
    $idCanco = rand(1, 17);

    $sqlCheckUser = "SELECT * FROM usuaris_songs WHERE idusuari = $idUsuari";
    $resultCheckUser = $conn->query($sqlCheckUser);

    if ($resultCheckUser->num_rows == 0) {
        $sqlInsertUser = "INSERT INTO usuaris_songs (idusuari, reproduccions) VALUES ($idUsuari, '[]')";
        $conn->query($sqlInsertUser);
        echo "S’ha afegit un nou registre a la BD per les reproduccions de l’usuari.";
    } else {
        $sqlCheckSong = "SELECT reproduccions FROM usuaris_songs WHERE idusuari = $idUsuari";
        $resultCheckSong = $conn->query($sqlCheckSong);
        $row = $resultCheckSong->fetch_assoc();
        $reproduccionsData = json_decode($row['reproduccions'], true);

        $found = false;

        foreach ($reproduccionsData as &$reproduccio) {
            if ($reproduccio['song'] == $idCanco) {
                $reproduccio['reproduccions']++;
                $found = true;
                break;
            }
        }

        if (!$found) {
            $reproduccionsData[] = ['song' => $idCanco, 'reproduccions' => 1];

            $sqlUpdateSongs = "UPDATE songs SET Reproduccions = Reproduccions + 1, Recaptacio = Recaptacio + 2 WHERE idSongs = $idCanco";
            $conn->query($sqlUpdateSongs);

            $sqlUpdateUser = "UPDATE usuaris_songs SET reproduccions = '" . json_encode($reproduccionsData) . "' WHERE idusuari = $idUsuari";
            $conn->query($sqlUpdateUser);

            echo "S’ha afegit una nova reproducció a la taula intermitja.";
        } else {
            $sqlUpdateSongs = "UPDATE songs SET Reproduccions = Reproduccions + 1, Recaptacio = Recaptacio + 2 WHERE idSongs = $idCanco";
            $conn->query($sqlUpdateSongs);

            echo "L’usuari ha incrementat les reproduccions d’aquesta cançó.";
        }
    }
} else {
    echo "S’ha produït un error.";
}

$conn->close();
?>
