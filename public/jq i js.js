
function buscarReproduccions() {
    let nomCanco = $('#inputCanco').val();

    $.ajax({
        url: 'phpsql.php',
        method: 'POST',
        dataType: 'json',
        data: { nomCanco: nomCanco },
        success: function(data) {
            console.log(data);
            mostrarResultats(data);
        },
    });
}

function mostrarResultats(data) {
    let resultatsDiv = $('#resultats');
    resultatsDiv.empty();

    if (data.length === 0) {
        resultatsDiv.text('No s\'han trobat resultats.');
    } else {
        let resultatsText = '';
        data.forEach(function(item) {
            resultatsText += 'ID Cançó: ' + item.idSongs + ', Nom Cançó: ' + item.Nom + ', Recaptació: ' + item.Recaptacio + '\n';
        });
        resultatsDiv.text(resultatsText);
    }
}
















function afegirNovaReproduccio() {
    $.ajax({
        type: 'POST',
        url: 'afegirsql.php',
        data: { novaReproduccio: true },
        success: function(response) {
            $('#resultatNovaCanco').text(response);
        },
        error: function(error) {
            $('#resultatNovaCanco').text('S\'ha produït un error: ' + error.statusText);
        }
    });
}


function afegirNovaCanco() {
    var nomNovaCanco = document.getElementById('nomNovaCanco').value;

    $.ajax({
        type: 'POST',
        url: 'phpsql2.php',
        data: { nomNovaCanco: nomNovaCanco },
        success: function(response) {
            $('#resultatNovaCanco').text(response);
        },
        error: function(error) {
            $('#resultatNovaCanco').text('S\'ha produït un error: ' + error.statusText);
        }
    });
}




function moureimg() {
    let imatge = document.getElementById("movi1");

    let nuevoAncho = imatge.width * 0.9;
    let nuevoAlto = imatge.height * 0.8;

    imatge.style.width = nuevoAncho + "px";
    imatge.style.height = nuevoAlto + "px";
}




$(document).ready(function(){

    $("#myCarousel").carousel();

    $(".item1").click(function(){
        $("#myCarousel").carousel(0);
    });
    $(".item2").click(function(){
        $("#myCarousel").carousel(1);
    });
    $(".item3").click(function(){
        $("#myCarousel").carousel(2);
    });
    $(".item4").click(function(){
        $("#myCarousel").carousel(3);
    });

    // Enable Carousel Controls
    $(".left").click(function(){
        $("#myCarousel").carousel("prev");
    });
    $(".right").click(function(){
        $("#myCarousel").carousel("next");
    });
});

$(document).ready(function () {
    $('body').css('background-image', 'url("https://w.forfun.com/fetch/79/797fe4458756bea9dfe6c5d1bc96ce89.jpeg?w=1470&r=0.5625")');
});

$(document).ready(function() {
    setInterval( function() {
        let hores = new Date().getHours();
        $(".hores").html(( hores < 10 ? "0" : "" ) + hores);
    }, 1000);
    setInterval( function() {
        let minuts = new Date().getMinutes();
        $(".minuts").html(( minuts < 10 ? "0" : "" ) + minuts);
    },1000);
    setInterval( function() {
        let segons = new Date().getSeconds();
        $(".segons").html(( segons < 10 ? "0" : "" ) + segons);
    },1000);
});

function actualitzar() {
    let dataActual = new Date();
    let hores = formatDosDigits(dataActual.getHours());
    let minuts = formatDosDigits(dataActual.getMinutes());
    let segons = formatDosDigits(dataActual.getSeconds());

    let rellot = document.getElementById("rellotge");
    rellot.textContent = `${hores}:${minuts}:${segons}`;
}

function formatDosDigits(valor) {
    return valor < 10 ? "0" + valor : valor;
}

setInterval(actualitzar, 1000);

let dadesPersonatges = [
    { nom: "Goku", foto: "https://i.pinimg.com/1200x/c7/4c/a6/c74ca6827eed655aa080143a0970242f.jpg", familia: "Chi-Chi Gohan, Goten, Raditz, Bardock, Pan", temporades: "Totes", descripcio: "Goku (en el seu planeta natal es diu Kakarot (カカロットEn japo, Kakarotto)) és el personatge principal del manga i anime Bola de Drac, creat per Akira Toriyama, es un guerrer extraterrestre de la raça dels saiyajins provinent del planeta vegeta." },
    { nom: "Vegeta", foto: "https://i.pinimg.com/474x/b6/98/6b/b6986bc2a9ef1aff4d9250ff6641b8a4.jpg", familia: "[Sayayin, Bulma, Trunks]", temporades: "Totes", descripcio: "Vegeta va néixer al voltant de l'any 732. El seu pare, el Rei Vegeta, era el rei dels Saiyan, convertint Vegeta en el seu príncep. En la seva joventut, Vegeta va veure com el seu pare havia de deixar el seu orgull de banda davant del Déu de la Destrucció, Beerus, per protegir la seva espècie. Novament l'espècie Saiyan es va veure vulnerable a Freezer, convertint Vegeta en un dels soldats més fidels a Freezer, encara que ell ocultava les seves veritables intencions davant d'ell." },
    { nom: "Gohan", foto: "https://media.fortniteapi.io/images/styles/T-Soldier-Hero-ArcticlceBlue.png", familia: "Chi-Chi Gohan, Goten, Raditz, Bardock", temporades: "Totes", descripcio: "És el primer fill de Són Gokū i Chi-Chi, germà major de Són Goten, espòs de Videl, amb qui té una filla cridada Pan. És meitat Saiyajin i meitat humà. El nom de Gohan és un joc de paraules entre la paraula 'gohan' (御飯, menjar, arròs en japonès) i el nom del seu pare." },
    { nom: "Goten", foto: "https://images-wixmp-ed30a86b8c4ca887773594c2.wixmp.com/f/84dc13b7-a2e7-4b45-83ec-311e72e82900/dbt9pul-165837d3-96cf-497f-a474-2304122992d5.png?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1cm46YXBwOjdlMGQxODg5ODIyNjQzNzNhNWYwZDQxNWVhMGQyNmUwIiwiaXNzIjoidXJuOmFwcDo3ZTBkMTg4OTgyMjY0MzczYTVmMGQ0MTVlYTBkMjZlMCIsIm9iaiI6W1t7InBhdGgiOiJcL2ZcLzg0ZGMxM2I3LWEyZTctNGI0NS04M2VjLTMxMWU3MmU4MjkwMFwvZGJ0OXB1bC0xNjU4MzdkMy05NmNmLTQ5N2YtYTQ3NC0yMzA0MTIyOTkyZDUucG5nIn1dXSwiYXVkIjpbInVybjpzZXJ2aWNlOmZpbGUuZG93bmxvYWQiXX0.pHZA3Rk1GJHbdefsTgFlypeEslSkcGR_S92ppXI2A3M", familia: "Chi-Chi, Goku, Goten, Raditz, Bardock, Pan", temporades: "Totes", descripcio: "Goten és molt semblant al seu pare, amb el mateix estil de pentinat que Són Goku, un rostre rialler i pell de rostre rosat heretat directament de la seva mare. En les seves primeres aparicions, portava un changshan blau amb vores verdes amb unes mànigues llargues i pantalons morats, a més dels seus pantalons surt una tela blanca. Durant el seu entrenament amb Gohan per al Torneig Mundial de les Arts Marcials." },
    { nom: "Bills", foto: "https://static.myfigurecollection.net/upload/items/1/304281-f877c.jpg", familia: "Sensa familia", temporades: "Dragon Ball Super", descripcio: "Bills és el déu de la destrucció de l'univers setè, i la zona administrativa inclou el planeta Terra. Ell dorm des de fa uns quants anys a diverses dècades consecutives, i ell destrueix planetes de la seva zona administrativa durant els pocs anys, quan està despert." },
    { nom: "Wish", foto: "https://pm1.aminoapps.com/6436/4d2b95065bb728f132ffa17fdf2d4dff5279efb8_128.jpg", familia: "Sensa familia", temporades: "Dragon ball super", descripcio: "És un personatge de pell de color celeste clar, pèl blanc i llarg i té característiques femenines i elegants. Està vestit amb una túnica color granat, una mena de cuirassa color negre i blanc amb detalls grocs, ataronjats i blancs, sabates de tac negre i porta un cèrcol de color blau que envolta el seu coll." },
    { nom: "Trunks", foto: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQWn8x_S25gHuEVTHoKYPHFVYKtCk_gNCQG7Q&usqp=CAU", familia: "Bulma de mare  pare Vegeta i Bra Germa", temporades: "Dragon ball Gt i super", descripcio: "És meitat terrícola i meitat saiyajin (Híbrid Humà-Saiyajin), fill de Vegeta i Bulma i germà gran de Bra en el present. En el futur alternatiu és l'únic saiyajin que queda com l'heroi de la terra (i no té germans o germanes)." },
    { nom: "Frezzer", foto: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTuVFSqZF-3dW-o5XdB_9gNCg7r07bg_tsslQ&usqp=CAU", familia: "Rey Cold (pare) Germa:Cooler", temporades: "Dragon ball Z i super", descripcio: "Freezer es un gobernante de un imperio galáctico que conquista y comercia con planetas. Es un tirano que gobernó con puño de hierro, destruyó el planeta Vegeta por temor al poder del legendario Super Saiyan y Super Saiyan dios"},
    {
            nom: "Majin Boo",
            foto: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHnf2iNKsbTRZWiY0JjSxkO8hNqNX5qIqsSQ&usqp=CAU",
            familia: "Monstre de 'l'infern'",
            temporades: "A partir de Dragon Ball Z totes les temporades",
            descripcio: "Majin Boo era originalment Buu, controlat per Bibidi. Durant la destrucció que Buu causava als planetes, Els Suprem Kaiō Shin van intervenir, durant aquestes batalles Buu mata als Kaiō Shin del Nord i el Kaiō Shin de l'Oest. Durant la seva lluita contra el Kaiō Shin del Sud, ell ho absorbeix i es converteix en Ultra Buu; quan Ultra Buu està a punt de matar al Kaiō Shin de l'Est, Dai Kaiō Shin intervé i és absorbit per Buu."
        },
    { nom: "Célula", foto:"https://a.wattpad.com/useravatar/DacsAlonsoCuellar.256.80532.jpg", familia: "Monstre de 'l'infern'",
        temporades: "A partir de Dragon Ball Z totes les temporades",
        descripcio: "Majin Boo era originalment Buu, controlat per Bibidi. Durant la destrucció que Buu causava als planetes, Els Suprem Kaiō Shin van intervenir, durant aquestes batalles Buu mata als Kaiō Shin del Nord i el Kaiō Shin de l'Oest. Durant la seva lluita contra el Kaiō Shin del Sud, ell ho absorbeix i es converteix en Ultra Buu; quan Ultra Buu està a punt de matar al Kaiō Shin de l'Est, Dai Kaiō Shin intervé i és absorbit per Buu."
    }

];

let taula = document.getElementById("taula");
let cosTaula = taula.getElementsByTagName("tbody")[0];

dadesPersonatges.forEach(personatge => {
    const fila = cosTaula.insertRow();

    Object.values(personatge).forEach(valor => {
        let cella = fila.insertCell();
        if (valor.includes("http")) {
            let imatge = document.createElement("img");
            imatge.src = valor;
            imatge.alt = personatge.nom;
            imatge.className = "timg";
            cella.appendChild(imatge);
        } else {
            cella.textContent = valor;
        }
    });
});

$(document).ready(function(){
    $(document).on('click', '.responsive', function(){
        $(this).toggleClass('rotate');
    });
});

$(document).ready(function(){
    function afegirFila(imatge, descripcio, any) {
        var fila = '<tr><td><img src="' + imatge + '" alt="Imatge"></td><td>' + descripcio + '</td><td>' + any + '</td></tr>';
        $('#miTaula tbody').append(fila);
    }

    afegirFila('https://via.placeholder.com/50x50', 'Descripció 1', '2022');

    $('#formulariFila').submit(function(event) {
        event.preventDefault();

        var novaImatge = $('#imatge').val();
        var novaDescripcio = $('#descripcio').val();
        var nouAny = $('#any').val();

        afegirFila(novaImatge, novaDescripcio, nouAny);

        $('#formulariFila')[0].reset();
    });
});




